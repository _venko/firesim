use crate::color::*;
use crate::constants::*;

#[derive(Clone, Debug)]
pub struct Cell {
    // The total thermal energy in this cell (J)
    pub energy: f64,
    // The specific heat of this cell (J/K)
    pub spec_heat: f64,
    // The amount of water in this cell (kg)
    pub water: f64,
    // The amount of grass in this cell (kg)
    pub grass: f64,
    // The amount of tree in this cell (kg)
    pub tree: f64,
    // The amount of coal in this cell (kg)
    pub coal: f64,
    // The amount of ash in this cell (kg)
    pub ash: f64,

    // The change in matter that has occured in this cell since the last time
    // this field was reset
    pub matter_error: f64,
    // The change in energy that has occured in this cell since the last time
    // this field was reset
    pub energy_error: f64,
}

impl Default for Cell {
    fn default() -> Cell {
        return Cell {
            energy: f64::default(),
            spec_heat: 1.0,
            water: f64::default(),
            grass: f64::default(),
            tree: f64::default(),
            coal: f64::default(),
            ash: f64::default(),

            matter_error: std::f64::INFINITY,
            energy_error: f64::default(),
        };
    }
}

impl Cell {
    // Invalidates this cell in response to a change in composition
    pub fn invalidate_matter(&mut self, delta: f64) {
        self.spec_heat = self.water * WATER_SPEC_HEAT
            + self.grass * GRASS_SPEC_HEAT
            + self.tree * TREE_SPEC_HEAT
            + self.coal * COAL_SPEC_HEAT
            + self.ash * ASH_SPEC_HEAT;
        self.matter_error += delta;
    }

    // Invalidates this cell in response to a change in energy
    pub fn invalidate_energy(&mut self, delta: f64) {
        self.energy_error += delta.abs();
    }

    // Gets the temperature of a cell
    pub fn get_temp(&self) -> f64 {
        return self.energy / self.spec_heat;
    }

    // Sets the temperature of a cell
    pub fn set_temp(&mut self, temp: f64) {
        self.energy = temp * self.spec_heat;
    }

    // Adds or subtracts energy to this cell
    pub fn exchange(&mut self, energy: f64) {
        self.energy = f64::max(0.0, self.energy + energy);
        self.invalidate_energy(energy);
    }

    // Independently updates this cell by the given amount of time (sec)
    pub fn update(&mut self, area: f64, dt: f64) {
        let temp = self.get_temp();

        // Evaporation
        if temp > EVAPORATION_CONST && self.water > 0.0 {
            let extra_energy = self.energy - EVAPORATION_CONST * self.spec_heat;
            let efficiency = f64::powf(0.5, dt);

            let energy_delta = extra_energy * efficiency;
            self.energy -= energy_delta;
            self.invalidate_energy(energy_delta);

            let vaporized = energy_delta / WATER_ENTHALPY_OF_VAPORIZATION;
            self.water = f64::max(0.0, self.water - vaporized);
            self.invalidate_matter(vaporized);
        }

        // Pyrolysis
        if temp > PYROLYSIS_CONST && self.tree > 0.0 {
            let rate = f64::powf((temp - PYROLYSIS_CONST) / 200.0, 0.3) * self.tree;
            let mass = rate * dt;

            self.tree = f64::max(0.0, self.tree - mass);
            self.coal += mass;
            self.invalidate_matter(mass * 2.0);
        }

        // Burning
        if temp > BURN_CONST {
            let burn_rate = ((temp - BURN_CONST) / 200.0).powf(0.3);
            let grass_rate = self.grass * burn_rate * GRASS_BURN_RATE;
            let tree_rate = self.tree * burn_rate * TREE_BURN_RATE;
            let coal_rate = self.coal * burn_rate * COAL_BURN_RATE;
            let grass_energy_rate = grass_rate * GRASS_ENERGY;
            let tree_energy_rate = tree_rate * TREE_ENERGY;
            let coal_energy_rate = coal_rate * COAL_ENERGY;
            let total_energy_rate = grass_energy_rate + tree_energy_rate + coal_energy_rate;

            let burn_capacity = BURN_CAPACITY * area;
            let efficiency = burn_capacity / (total_energy_rate + burn_capacity);

            self.energy += total_energy_rate * efficiency * dt;
            self.grass = f64::max(0.0, self.grass - grass_rate * efficiency * dt);
            self.tree = f64::max(0.0, self.tree - tree_rate * efficiency * dt);
            self.coal = f64::max(0.0, self.coal - coal_rate * efficiency * dt);
            self.ash += (grass_rate + tree_rate + coal_rate) * efficiency * dt;

            self.invalidate_matter((grass_rate + tree_rate + coal_rate) * 2.0 * efficiency * dt);
            self.invalidate_energy(total_energy_rate * efficiency * dt);
        }
    }

    pub fn to_color(&self, scale: f64) -> Color {
        let mut mix = scale * scale * 0.2;
        let mut col = GROUND_COLOR * mix;

        mix += self.water + (self.grass * 3.0) + self.tree + self.coal + self.ash;
        col += WATER_COLOR * self.water;
        col += GRASS_COLOR * self.grass * 3.0;
        col += TREE_COLOR * self.tree;
        col += COAL_COLOR * self.coal;
        col += ASH_COLOR * self.ash;
        col *= 1.0 / mix;

        let temp = self.get_temp();
        if temp > BURN_CONST {
            mix = 1.0;
            let hot_mix = (temp - BURN_CONST) / BURN_CONST;

            col += HOT_COLOR * hot_mix;
            mix += hot_mix;

            if temp > 1400.0 {
                let really_hot_mix = (temp - 1400.0) / 400.0;
                col += REALLY_HOT_COLOR * really_hot_mix;
                mix += really_hot_mix;
            }
            col *= 1.0 / mix;
        }

        return col;
    }
}
