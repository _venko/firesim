use std::ops::{Add, AddAssign, Mul, MulAssign};

// Represents an RGB color
#[derive(Debug)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

// Macro for making colors
macro_rules! color {
    ($r:expr, $g:expr, $b:expr) => {
        Color {
            r: $r,
            g: $g,
            b: $b,
        }
    };
}

impl Add for Color {
    type Output = Color;

    fn add(self, other: Color) -> Color {
        return color!(self.r + other.r, self.g + other.g, self.b + other.b);
    }
}

impl AddAssign for Color {
    fn add_assign(&mut self, other: Color) {
        *self = color!(self.r + other.r, self.g + other.g, self.b + other.b);
    }
}

impl Mul<f64> for Color {
    type Output = Color;

    fn mul(self, other: f64) -> Color {
        return color!(self.r * other, self.g * other, self.b * other);
    }
}

impl MulAssign<f64> for Color {
    fn mul_assign(&mut self, other: f64) {
        *self = color!(self.r * other, self.g * other, self.b * other);
    }
}

// Some base color constants for mixing during cell rendering
pub const GROUND_COLOR: Color = color!(0.0, 0.0, 0.0);
pub const WATER_COLOR: Color = color!(0.0, 0.0, 1.0);
pub const GRASS_COLOR: Color = color!(0.3, 0.7, 0.1);
pub const TREE_COLOR: Color = color!(0.1, 0.6, 0.2);
pub const COAL_COLOR: Color = color!(0.3, 0.3, 0.3);
pub const ASH_COLOR: Color = color!(0.7, 0.7, 0.7);
pub const HOT_COLOR: Color = color!(1.0, 0.2, 0.0);
pub const REALLY_HOT_COLOR: Color = color!(1.0, 0.9, 0.6);
