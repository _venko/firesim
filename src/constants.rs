// The specific heat of water (J/kg*K)
pub const WATER_SPEC_HEAT: f64 = 4180.0;
// The enthalpy of vaporization for water (J/kg)
pub const WATER_ENTHALPY_OF_VAPORIZATION: f64 = 2.26e6;

// The burn rate of grass (exposed area)
pub const GRASS_BURN_RATE: f64 = 2.5;
// The energy content of grass (J/kg*K)
pub const GRASS_ENERGY: f64 = 1.70e7;
// The specific heat of grass (J/kg*K)
pub const GRASS_SPEC_HEAT: f64 = 300.0;

// The burn rate of tree (exposed area)
pub const TREE_BURN_RATE: f64 = 0.05;
// The energy content of tree (J/kg*K)
pub const TREE_ENERGY: f64 = 1.62e7;
// The specific heat of tree (J/kg*K)
pub const TREE_SPEC_HEAT: f64 = 200.0;

// The burn rate of coal (exposed area)
pub const COAL_BURN_RATE: f64 = 0.02;
// The energy content of coal (J/kg*K)
pub const COAL_ENERGY: f64 = 2.40e7;
// The specific heat of coal (J/kg*K)
pub const COAL_SPEC_HEAT: f64 = 200.0;

// The specific heat of ash (J/kg*K)
pub const ASH_SPEC_HEAT: f64 = 350.0;

// The maximum combustion rate for a burning cell (W/m^2)
pub const BURN_CAPACITY: f64 = 1.0e7;

pub const EVAPORATION_CONST: f64 = 373.0;
pub const PYROLYSIS_CONST: f64 = 700.0;
pub const BURN_CONST: f64 = 600.0;

// The Stefan-Boltzmann constant (W/m^2*K^4)
pub const STEFAN_BOLTZMANN: f64 = 5.67e-8;

// The convective heat transfer that occurs between a cell and the air above (W/K*m^2)
pub const VERT_HEAT_TRANSFER: f64 = 5.0e2;

// The convective heat transfer that occurs between two adjacent cells (W/K)
pub const HORIZ_HEAT_TRANSFER: f64 = 5.0e2;
