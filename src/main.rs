extern crate bmp;
extern crate image;
extern crate piston_window;
extern crate rand;
extern crate rayon;
extern crate time;

#[macro_use]
mod color;
mod cell;
mod constants;
mod world;

use piston_window::*;
use std::env;
use world::*;

fn main() {
    let mut world: World = {
        let args: Vec<String> = env::args().collect();
        let img_path = &args[1];
        println!("{}", img_path);
        let bmp = bmp::open(img_path).unwrap_or_else(|e| {
            panic!("Failed to open: {}", e);
        });
        println!("loaded a {}x{} bmp", bmp.get_width(), bmp.get_height());
        World::from_bitmap(bmp, 1.0)
    };

    let width: usize = world.get_width();
    let height: usize = world.get_height();
    let scale: usize = 1;

    let mut window: PistonWindow =
        WindowSettings::new("firesim", [(width * scale) as u32, (height * scale) as u32])
            .exit_on_esc(true)
            .build()
            .unwrap();
    let mut canvas = image::ImageBuffer::new(width as u32, height as u32);
    let mut texture: G2dTexture =
        Texture::from_image(&mut window.factory, &canvas, &TextureSettings::new()).unwrap();

    let mut mouse_pos = [0, 0];
    let mut timestamp = time::precise_time_s();
    let mut ticks = 0;
    while let Some(event) = window.next() {
        if let Some(_) = event.render_args() {
            world.to_image_buffer(&mut canvas);
            texture.update(&mut window.encoder, &canvas).unwrap();
            window.draw_2d(&event, |context, graphics| {
                image(
                    &texture,
                    context.transform.scale(scale as f64, scale as f64),
                    graphics,
                );
            });
        }

        if let Some(pos) = event.mouse_cursor_args() {
            mouse_pos[0] = pos[0] as usize;
            mouse_pos[1] = pos[1] as usize;
        }

        if let Some(button) = event.press_args() {
            let x = mouse_pos[0] / scale;
            let y = mouse_pos[1] / scale;
            match button {
                Button::Mouse(MouseButton::Left) => {
                    println!("{}: igniting {},{}", ticks, x, y);
                    world.ignite_splotch(x, y, 10.0, 1000.0);
                }
                Button::Mouse(MouseButton::Right) => {
                    println!("{}: splashing {},{}", ticks, x, y);
                    world.splash_splotch(x, y, 10.0, 1.0);
                }
                _ => {}
            }
        }

        let now = time::precise_time_s();
        let duration = now - timestamp;
        timestamp = now;
        world.update(f64::min(1.0 / 60.0, duration));
        ticks += 1;
    }
}
