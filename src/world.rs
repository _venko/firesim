use bmp::Image;
use image::RgbaImage;
use rand::prelude::*;
use rayon::prelude::*;

use crate::cell::Cell;
use crate::constants::*;

// The length of a cell (m)
const CELL_SIZE: f64 = 1.0;

pub struct World {
    // The length of a cell in this world (m)
    scale: f64,
    // The ambient tempurature for this world (K)
    ambient_temp: f64,
    // The cell grid for this world
    pub cells: Vec<Vec<Cell>>,
}

impl World {
    // Creates an empty world with the given scale and dimensions
    // Creates a world based on a bitmap
    pub fn from_bitmap(img: Image, scale: f64) -> World {
        let width = img.get_width();
        let height = img.get_height();
        let cell_area = scale * scale;
        let mut rng = rand::thread_rng();

        let mut grid = vec![vec![Cell::default(); width as usize]; height as usize];
        for row in 0..height {
            for col in 0..width {
                let pixel = img.get_pixel(col, row);
                let grass =
                    pixel.g as f64 / 256.0 * cell_area * 0.1 * (3.0 + rng.gen_range(0.0, 1.0))
                        / 3.0;
                let tree =
                    pixel.b as f64 / 256.0 * cell_area * 4.0 * (3.0 + rng.gen_range(0.0, 1.0))
                        / 3.0;

                let mut cell = &mut grid[row as usize][col as usize];
                cell.grass = grass;
                cell.tree = tree;
                cell.water = tree * 0.1 + grass * 0.1;
                cell.invalidate_matter(std::f64::INFINITY);
            }
        }

        return World {
            scale: scale,
            ambient_temp: 0.0,
            cells: grid,
        };
    }

    // Gets the width dimension of a world
    pub fn get_width(&self) -> usize {
        return self.cells[0].len();
    }

    // Gets the height dimension of a world
    pub fn get_height(&self) -> usize {
        return self.cells.len();
    }

    // Performs a mutual exchange between the cells at the given coordinates
    // Coordinates are used instead of cell references because the compiler wont allow borrows from
    // the same vec simultaneously
    fn exchange_cells(&mut self, (ay, ax): (usize, usize), (by, bx): (usize, usize), dt: f64) {
        let diff = (self.cells[ay][ax]).get_temp() - (self.cells[by][bx]).get_temp();
        if diff.abs() > 0.001 {
            let transfer_rate = diff * HORIZ_HEAT_TRANSFER * dt;
            (self.cells[ay][ax]).exchange(-1.0 * transfer_rate);
            (self.cells[by][bx]).exchange(transfer_rate);
        }
    }

    // Performs mutual exchange between all cells in a world
    fn exchange_all(&mut self, dt: f64) {
        // Handle the first row separately to avoid integer wrapping errors
        {
            let row = 0;
            let next_row = row + 1;
            for col in 0..self.get_width() - 1 {
                let next_col = col + 1;
                self.exchange_cells((row, col), (next_row, col), dt);
                self.exchange_cells((row, col), (next_row, next_col), dt);
                self.exchange_cells((row, col), (row, next_col), dt);
            }
        }

        for row in 1..self.get_height() - 1 {
            let next_row = row + 1;
            let prev_row = row - 1;
            for col in 0..self.get_width() - 1 {
                let next_col = col + 1;
                self.exchange_cells((row, col), (next_row, col), dt);
                self.exchange_cells((row, col), (next_row, next_col), dt);
                self.exchange_cells((row, col), (row, next_col), dt);
                self.exchange_cells((row, col), (prev_row, next_col), dt);
            }
        }
    }

    // Updates a world by the given amount of time (sec)
    pub fn update(&mut self, dt: f64) {
        let amb_temp = self.ambient_temp;
        let cell_area = self.scale * self.scale;

        self.cells.par_iter_mut().for_each(|row| {
            row.par_iter_mut().for_each(|cell| {
                cell.update(cell_area, dt);

                // Convection
                cell.exchange((amb_temp - cell.get_temp()) * cell_area * VERT_HEAT_TRANSFER * dt);

                // Outflow radiation;
                let temperature = cell.get_temp();
                let k = STEFAN_BOLTZMANN * CELL_SIZE * CELL_SIZE / cell.spec_heat;
                let next_temperature = (3.0 * k * dt + temperature.powf(-3.0)).powf(-1.0 / 3.0);
                cell.set_temp(next_temperature);
                cell.invalidate_energy((temperature - next_temperature) * cell.spec_heat);
            })
        });

        self.exchange_all(dt);
    }

    // Alters all cells in a circular area surrounding the given cell using
    // the given operation.
    fn splotch<F>(&mut self, x: usize, y: usize, radius: f64, mut splotcher: F)
    where
        F: FnMut(&mut Cell, f64) -> (),
    {
        let crad = radius / self.scale;
        let start = f64::floor(-1.0 * crad) as isize;
        let end = f64::ceil(crad) as isize;
        for dy in start..=end {
            for dx in start..=end {
                let row = (y as isize) + dy;
                let col = (x as isize) + dx;
                let in_bounds = {
                    let row_in_bounds = 0 <= row && row < self.get_height() as isize;
                    let col_in_bounds = 0 <= col && col < self.get_width() as isize;
                    row_in_bounds && col_in_bounds
                };
                let dist = f64::sqrt((dy * dy + dx * dx) as f64);
                if in_bounds && dist < radius {
                    let cell = &mut self.cells[row as usize][col as usize];
                    let s = 1.0 - dist / radius;
                    splotcher(cell, s);
                    cell.invalidate_matter(std::f64::INFINITY);
                    cell.invalidate_energy(std::f64::INFINITY);
                }
            }
        }
    }

    // Increases the temperature in the given splotch region
    pub fn ignite_splotch(&mut self, x: usize, y: usize, radius: f64, temperature: f64) {
        let splotcher = |cell: &mut Cell, s: f64| {
            cell.set_temp(cell.get_temp() + s * temperature);
        };
        self.splotch(x, y, radius, splotcher);
    }

    // Adds the given mass of water (kg) over the given splotch region
    pub fn splash_splotch(&mut self, x: usize, y: usize, radius: f64, mass: f64) {
        let mass = mass / std::f64::consts::PI * self.scale * self.scale;
        let amb_temp = self.ambient_temp;
        let splotcher = |cell: &mut Cell, s: f64| {
            let m = s * mass;
            cell.water += m;
            cell.energy += amb_temp * WATER_SPEC_HEAT * m;
        };
        self.splotch(x, y, radius, splotcher);
    }

    // Populates the given buffer with the RGB representation of a world
    pub fn to_image_buffer(&mut self, buffer: &mut RgbaImage) {
        let scale = self.scale;
        let mut rng = rand::thread_rng();
        for (x, y, pixel) in buffer.enumerate_pixels_mut() {
            let mut cell = &mut self.cells[y as usize][x as usize];
            if cell.matter_error >= 0.5 || cell.energy_error >= 1.0e3 {
                let grain = {
                    let temp = cell.get_temp();
                    let grain = rng.gen_range(0.0, f64::max(5.0, (temp - 200.0) / 50.0));
                    grain as u16
                };
                let (r, g, b) = {
                    let color = cell.to_color(scale);
                    let r = (color.r * 255.0) as u16;
                    let g = (color.g * 255.0) as u16;
                    let b = (color.b * 255.0) as u16;
                    (
                        u16::min(255, r + grain) as u8,
                        u16::min(255, g + grain) as u8,
                        u16::min(255, b + grain) as u8,
                    )
                };
                cell.energy_error = 0.0;
                cell.matter_error = 0.0;
                *pixel = image::Rgba([r, g, b, 255]);
            }
        }
    }
}
